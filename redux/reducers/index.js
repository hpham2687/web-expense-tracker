import { combineReducers } from 'redux';
import { uiReducer } from './ui';
// COMBINED REDUCERS
const reducers = {
  ui: uiReducer,
};

export default combineReducers(reducers);
