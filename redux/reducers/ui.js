// COUNTER REDUCER
import * as types from '../constants';

const initState = {
  isShowModalSetting: false,
};
export const uiReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case 'setShowModalSetting':
      return { ...state, isShowModalSetting: payload };
    case types.DECREMENT:
      return state - 1;
    case types.RESET:
      return 0;
    default:
      return state;
  }
};
