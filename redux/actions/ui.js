export const setShowModalSetting = (payload) => (dispatch) => dispatch({
  type: 'setShowModalSetting',
  payload,
});
