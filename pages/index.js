import Head from 'next/head';
import HomeContainer from '../containers/Home/index';
import styles from '../styles/Home.module.css';

export default function Home() {
  return (
    <HomeContainer />
  );
}
