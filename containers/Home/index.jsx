import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Row, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Overview from '../../components/Home/overview.jsx';
import BackupData from '../../components/Home/backup.jsx';
import SixHubOverview from '../../components/Home/sixHubOverview.jsx';
import ExpenseReport from '../../components/Home/expenseReport.jsx';
import LastestTransaction from '../../components/Home/lastestTransaction.jsx';
import Modals from '../../components/Modal/index.js';
import Header from '../../components/commons/header';

const HomeContainer = () => (

  <>
    <Modals />
    <div className="home-container">
      <Header />
      <Container className=" mt-3" fluid="md">
        <Row style={{ flexWrap: 'nowrap' }}>
          <Col className="mr-lg-3" xs={3}>
            <Row>
              <Col className="--whiteBackground" lg={12}>

                <Overview />
              </Col>
              <Col className="mt-3 --whiteBackground" lg={12}>

                <BackupData />
              </Col>
            </Row>
          </Col>
          <Col xs={9}>
            <Row>
              <Col className="--whiteBackground mb-3" xs={12}>
                <SixHubOverview />
              </Col>
            </Row>
            <Row style={{ flexWrap: 'nowrap' }}>
              <Col style={{ maxWidth: 'calc(66.666667% - 1rem)' }} className="--whiteBackground mr-lg-3" xs={8}>
                <ExpenseReport />
              </Col>
              <Col style={{ alignSelf: 'start' }} className="--whiteBackground" xs={4}>
                <LastestTransaction />
              </Col>
            </Row>

          </Col>

        </Row>
      </Container>

    </div>
  </>
);

export default HomeContainer;
