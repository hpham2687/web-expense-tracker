import React from 'react';
import Modal from '@material-ui/core/Modal';
import {
  AppBar, Box, Fade, Tab, Tabs, Typography,
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { setShowModalSetting } from '../../redux/actions/ui';

function getModalStyle() {
  return {
    top: '50%',
    left: '50%',
    zIndex: 333,
    transform: 'translate(-50%, -50%)',
  };
}

function rand() {
  return Math.round(Math.random() * 20) - 10;
}
const Modals = () => {
  const dispatch = useDispatch();

  const [modalStyle] = React.useState(getModalStyle);
  const open = useSelector((state) => state.ui.isShowModalSetting);
  const [value, setValue] = React.useState(0);
  console.log('reerender');

  const handleOpen = () => {
    dispatch(setShowModalSetting(true));
  };
  const handleClose = () => {
    dispatch(setShowModalSetting(false));
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  function TabPanel(props) {
    const {
      children, value, index, ...other
    } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }
  const body = (
    <div style={modalStyle} className="modals">
      <div className="modals__wrapper">

        <h2 className="modals__title">Settings</h2>
        <span onClick={handleClose} className="modals__close">
          <i className="fas fa-times" />
        </span>
        <div className="modals__content">
          <AppBar position="static">
            <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
              <Tab label="Item One" {...a11yProps(0)} />
              <Tab label="Item Two" {...a11yProps(1)} />
              <Tab label="Item Three" {...a11yProps(2)} />
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={0}>
            Item One
          </TabPanel>
          <TabPanel value={value} index={1}>
            Item Two
          </TabPanel>
          <TabPanel value={value} index={2}>
            Item Three
          </TabPanel>
        </div>

      </div>
    </div>
  );
  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Fade in={open}>
          {body}
        </Fade>
      </Modal>
    </>
  );
};

export default Modals;
