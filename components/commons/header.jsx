import {
  AppBar, Button, IconButton, makeStyles, Toolbar, Typography,
} from '@material-ui/core';
import React from 'react';
import MenuIcon from '@material-ui/icons/Menu';
import { useDispatch } from 'react-redux';
import { setShowModalSetting } from '../../redux/actions/ui';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#d9e5e8',
    height: '100vh',
    width: '100vw',
    padding: '20px',
  },
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  columns: {
    backgroundColor: '#fff',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },

}));
const Header = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <>
      {/* //header */}
      <AppBar color="primary" position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className="nav-link">
            Home
          </Typography>
          <Typography className="nav-link" variant="h6">
            Transaction
          </Typography>
          <Typography variant="h6" style={{ flexGrow: 1 }} className="nav-link">
            Money Report
          </Typography>
          <Button onClick={() => { dispatch(setShowModalSetting(true)); }} color="inherit">Settings</Button>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
      {/* //header */}
    </>
  );
};

export default Header;
