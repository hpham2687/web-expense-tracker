import React from 'react';
import { Pie } from 'react-chartjs-2';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const data = {
  labels: ['Earning', 'Expense'],
  datasets: [
    {
      label: '# of Votes',
      data: [50, 50],
      backgroundColor: [
        '#5e72e4',
        '#f5365c',
      ],
      borderColor: [
        '#5e72e4',
        '#f5365c',
      ],
      borderWidth: 1,
    },
  ],
};
const options = {
  cutoutPercentage: 50,
  aspectRatio: 0.5,
};

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
    padding: '0px 4px',
  },
}));
const Overview = () => {
  const classes = useStyles();

  return (
    <>
      <div className="overview-chart">

        <div className="overview-pie">

          <Pie width="100%" height="auto" data={data} options={options} />
        </div>
        <div className="overview-available-money">
          <span className="overview-available-money__text">
            Available Money
          </span>
          <span className="overview-available-money__amount">61000</span>
        </div>
        <div className="overview-action">
          <Button size="small" variant="contained" color="primary">
            <i className="fas fa-plus-circle" />
            {' '}
            Earning
            <span className="overview-action__amount">31000</span>
          </Button>
          <Button
            className={classes.button}
            variant="contained"
            color="secondary"
            size="small"
          >
            <i className="fas fa-minus-circle" />
            {' '}
            Expense
            <span className="overview-action__amount">30000</span>

          </Button>
        </div>
      </div>

    </>
  );
};

export default Overview;
