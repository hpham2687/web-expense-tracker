import { Button } from '@material-ui/core';
import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  textField: {
    width: '100%',
    marginTop: '0',
  },
}));

const BackupData = () => {
  const classes = useStyles();
  return (

    <>
      <div className="backup-tab">

        <h3 className="tab-heading">
          Backup data
        </h3>

        <div className="form-input-wrap">

          <TextField
            className={classes.textField}
            label="File Name"
            id="file_name"
            name="file_name"
            InputLabelProps={{
              shrink: true,
            }}
          />

        </div>
        <div className="form-input-wrap">

          <TextField
            id="from_date"
            type="date"
            label="From date"

            defaultValue="2017-05-24"
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          {' '}

        </div>
        <div className="form-input-wrap">

          <TextField
            id="to_date"
            type="date"
            label="To Date"

            defaultValue="2017-05-24"
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          {' '}

        </div>

        <Button size="small" variant="contained" color="primary">
          <i style={{ marginRight: '6px' }} className="fas fa-sync-alt" />
          Get Data
        </Button>

      </div>
    </>
  );
};

export default BackupData;
