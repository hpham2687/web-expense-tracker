import React from 'react';
import { Pie } from 'react-chartjs-2';

const data = [
  {
    datasets: [
      {
        label: '# of Votes',
        data: [50, 50],
        backgroundColor: [
          'white',
          '#5e72e4',
        ],
        borderColor: [
          '#5e72e4',
          '#5e72e4',
        ],
        borderWidth: 1,
      }],
    labels: [
      'Earning', 'Expense',
    ],
  },
  {
    datasets: [

      {
        label: '# of Votes',
        data: [50, 50],
        backgroundColor: [
          'white',
          '#2bce89',
        ],
        borderColor: [
          '#2bce89',
          '#2bce89',
        ],
        borderWidth: 1,
      }],
    labels: [
      'Earning', 'Expense',
    ],
  },
  {
    datasets: [
      {
        label: '# of Votes',
        data: [50, 50],
        backgroundColor: [
          'white',
          '#10cdef',
        ],
        borderColor: [
          '#10cdef',
          '#10cdef',
        ],
        borderWidth: 1,
      }],
    labels: [
      'Earning', 'Expense',
    ],
  },
  {
    datasets: [

      {
        label: '# of Votes',
        data: [50, 50],
        backgroundColor: [
          'white',
          '#182c4d',
        ],
        borderColor: [
          '#182c4d',
          '#182c4d',
        ],
        borderWidth: 1,
      }],
    labels: [
      'Earning', 'Expense',
    ],
  },
  {
    datasets: [

      {
        label: '# of Votes',
        data: [50, 50],
        backgroundColor: [
          'white',
          '#fb6340',
        ],
        borderColor: [
          '#fb6340',
          '#fb6340',
        ],
        borderWidth: 1,
      }],
    labels: [
      'Earning', 'Expense',
    ],
  },
  {
    datasets: [

      {

        data: [60, 40],
        backgroundColor: [
          'white',
          '#f5365c',
        ],
        borderColor: [
          '#f5365c',
          '#f5365c',
        ],
        borderWidth: 1,
      }],
    labels: [
      'Earning', 'Expense',
    ],
  },

];
const options = {
  cutoutPercentage: 50,
  legend: {
    display: false,
  },

  aspectRatio: 0.5,
};

const SixHubOverview = () => (
  <>
    <div className="six-hub-overview">
      <ul className="six-hub-overview__list">
        <li className="six-hub-overview__item">
          <Pie width="100%" height="auto" data={data[0]} options={options} />
          <div className="six-hub-overview__title">Essential</div>
          <span className="six-hub-overview__amount">30000</span>
        </li>
        <li className="six-hub-overview__item">
          {' '}
          <Pie width="100%" height="auto" data={data[1]} options={options} />
          <div className="six-hub-overview__title">Education</div>
          <span className="six-hub-overview__amount">30000</span>
        </li>
        <li className="six-hub-overview__item">
          {' '}
          <Pie width="100%" height="auto" data={data[2]} options={options} />
          <div className="six-hub-overview__title">
            SAVING
          </div>
          <span className="six-hub-overview__amount">30000</span>
        </li>
        <li className="six-hub-overview__item">
          {' '}
          <Pie width="100%" height="auto" data={data[3]} options={options} />
          <div className="six-hub-overview__title">ENJOYING</div>
          <span className="six-hub-overview__amount">30000</span>
        </li>
        <li className="six-hub-overview__item">
          {' '}
          <Pie width="100%" height="auto" data={data[4]} options={options} />
          <div className="six-hub-overview__title">Investment</div>
          <span className="six-hub-overview__amount">30000</span>
        </li>
        <li className="six-hub-overview__item">
          {' '}
          <Pie width="100%" height="auto" data={data[5]} options={options} />
          <div className="six-hub-overview__title">Charity</div>
          <span className="six-hub-overview__amount">30000</span>
        </li>
      </ul>
    </div>
  </>
);

export default SixHubOverview;
