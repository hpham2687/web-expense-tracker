import React from 'react';

const LastestTransaction = () => (
  <>
    <div className="latest-trans">
      <ul className="latest-trans__list">
        <li className="latest-trans__item">
          <div className="latest-trans__type">
            <div className="latest-trans__icon">

              <i className="fa fa-flask" />
            </div>
            <h3 className="latest-trans__title">Education</h3>
          </div>
          <div className="latest-trans__content">
            <h4 className="latest-trans__amount">+3999</h4>
            <span className="latest-trans__date">32/3/2000</span>
          </div>
        </li>
        <li className="latest-trans__item">
          <div className="latest-trans__type">
            <div className="latest-trans__icon">

              <i className="fa fa-flask" />
            </div>
            <h3 className="latest-trans__title">Tiet Kiem</h3>
          </div>
          <div className="latest-trans__content">
            <h4 className="latest-trans__amount">+3999</h4>
            <span className="latest-trans__date">32/3/2000</span>
          </div>
        </li>
        <li className="latest-trans__item">
          <div className="latest-trans__type">
            <div className="latest-trans__icon">

              <i className="fa fa-flask" />
            </div>
            <h3 className="latest-trans__title">Tiet Kiem</h3>
          </div>
          <div className="latest-trans__content">
            <h4 className="latest-trans__amount">+3999</h4>
            <span className="latest-trans__date">32/3/2000</span>
          </div>
        </li>
        <li className="latest-trans__item">
          <div className="latest-trans__type">
            <div className="latest-trans__icon">

              <i className="fa fa-flask" />
            </div>
            <h3 className="latest-trans__title">Tiet Kiem</h3>
          </div>
          <div className="latest-trans__content">
            <h4 className="latest-trans__amount">+3999</h4>
            <span className="latest-trans__date">32/3/2000</span>
          </div>
        </li>
        <li className="latest-trans__item">
          <div className="latest-trans__type">
            <div className="latest-trans__icon">

              <i className="fa fa-flask" />
            </div>
            <h3 className="latest-trans__title">Tiet Kiem</h3>
          </div>
          <div className="latest-trans__content">
            <h4 className="latest-trans__amount">+3999</h4>
            <span className="latest-trans__date">32/3/2000</span>
          </div>
        </li>

      </ul>
    </div>
  </>
);

export default LastestTransaction;
