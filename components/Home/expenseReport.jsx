import { Button } from '@material-ui/core';
import React from 'react';
import { Line } from 'react-chartjs-2';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  button: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
  },
}));
const data = {
  labels: ['day 1', 'day final'],
  datasets: [
    {
      label: ['Earning'],
      data: [12, 19],
      fill: false,
      backgroundColor: 'rgb(255, 99, 132)',
      borderColor: 'rgba(255, 99, 132, 0.2)',
    },
    {
      label: ['Expense'],
      data: [10, 33],
      fill: false,
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'rgb(255, 255, 0)',
    },
  ],
};

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
};

const ExpenseReport = () => {
  const classes = useStyles();

  return (

    <>
      <div className="expense-report">

        <h3 className="tab-heading">
          Expense Report
        </h3>
        <div className="expense-report__chart">

          <Line data={data} options={options} />
        </div>
        <Button className={classes.button} size="small" variant="contained" color="primary">
          <span className="overview-action__amount">See detail</span>
        </Button>
      </div>
    </>
  );
};

export default ExpenseReport;
